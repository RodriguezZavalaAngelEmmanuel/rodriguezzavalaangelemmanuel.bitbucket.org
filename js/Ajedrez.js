var Ajedrez = (function(){
    
    
    
        
        var mostrar_tablero = function(){
            var filas = 8;
            var columnas = 8;
            
            var body = document.getElementById("tablero");
           
            
            var tabla   = document.createElement("table");
            var tblBody = document.createElement("tbody");
           
            
            for (var i = 0; i < filas; i++) {
              
              var hilera = document.createElement("tr");
           
              for (var j = 0; j < columnas; j++) {
                
                var celda = document.createElement("td");
                if(i%2 == 0){
                    if(j%2==0){
                        celda.setAttribute("class", "blancas");
    
                    }
                    else{
                        celda.setAttribute("class", "negras");
                    }

                }else{
                    if(j%2==0){
                        celda.setAttribute("class", "negras");
    
                    }
                    else{
                        celda.setAttribute("class", "blancas");
                    }
                }

                var textoCelda = " ";
                celda.textContent = textoCelda;
                hilera.appendChild(celda);
              }
           
              
              tblBody.appendChild(hilera);
            }
           
            tabla.appendChild(tblBody);
            tabla.setAttribute('id', 'tabla');
            body.appendChild(tabla);

            var arr_filas = document.getElementsByTagName('tr');

            var peones_negros = arr_filas[1];

            for(var i  = 0; i< peones_negros.children.length; i++){
                var peonnegro = peones_negros.children[i];
                peonnegro.textContent = "\u2659";
            }

            
            var peones_blancos = arr_filas[6];

            for(var i  = 0; i< peones_blancos.children.length; i++){
                var peonblanco = peones_blancos.children[i];
                peonblanco.textContent = "\u265f";
            }

            var lineas_negra = arr_filas[0];
            lineas_negra.children[0].textContent = "\u2656";
            lineas_negra.children[1].textContent = "\u2658";
            lineas_negra.children[2].textContent = "\u2657";
            lineas_negra.children[3].textContent = "\u2655";
            lineas_negra.children[4].textContent = "\u2654";
            lineas_negra.children[5].textContent = "\u2657";
            lineas_negra.children[6].textContent = "\u2658";
            lineas_negra.children[7].textContent = "\u2656";

            var lineas_blanca = arr_filas[7];
            lineas_blanca.children[0].textContent = "\u265c";
            lineas_blanca.children[1].textContent = "\u265e";
            lineas_blanca.children[2].textContent = "\u265d";
            lineas_blanca.children[3].textContent = "\u265b";
            lineas_blanca.children[4].textContent = "\u265a";
            lineas_blanca.children[5].textContent = "\u265d";
            lineas_blanca.children[6].textContent = "\u265e";
            lineas_blanca.children[7].textContent = "\u265c";

          }
          
          
          var asignarCoordenadas = function(){
            
              var numeros = [1,2,3,4,5,6,7,8];
              var letras = ['a','b','c','d','e','f','g','h'];
             

              var filas = document.getElementsByTagName('tr');

              for(var i = 0; i<filas.length; i++){
                  var numero = numeros[i];
                for(var j = 0; j< filas[0].children.length; j++){
                    var letra = letras[j];
                    var nombre = letra+numero;

                    var celda = filas[i].children[j];
                    celda.setAttribute("name", nombre);
                }
              }
          }

          var mover_pieza = function(objeto){
              asignarCoordenadas();

              var origen = objeto.de;
              var destino = objeto.a;

              var celda_origen = document.getElementsByName(origen)[0];
              var celda_destino = document.getElementsByName(destino)[0];

              var pieza = celda_origen.textContent;

              celda_origen.textContent = "";

              celda_destino.textContent = pieza;
          }
    
          
    
          return{
              "mostrarTablero": mostrar_tablero,
              "moverPieza": mover_pieza
              
          }
    })();
